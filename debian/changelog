abs-guide (10-3) unstable; urgency=medium

  * debian/control
    - update Vcs-* fields to point to salsa.d.o
    - bump Standards-Version to 4.3.0 (no changes needed)
  * debian/copyright
    - extend packaging copyright years
  * bump compat to 11

 -- Sandro Tosi <morph@debian.org>  Fri, 01 Feb 2019 19:42:38 -0500

abs-guide (10-2) unstable; urgency=medium

  * upload to unstable

 -- Sandro Tosi <morph@debian.org>  Sun, 02 Aug 2015 20:26:19 +0100

abs-guide (10-1) experimental; urgency=medium

  * New upstream release
  * debian/control
    - package can now be moved to main
  * debian/copyright
    - now the project is in public domain
    - switch to DEP-5
  * debian/source/format
    - switch to 3.0 (quilt) source format
  * debian/control
    - bump Standards-Version to 3.9.6 (no changes needed)
  * debian/abs-guide.lintian-overrides
    - removed an unused override
    - add some more override for example-shell-script-fails-syntax-check
  * debian/{compat, control}
    - switch to dh 9 compat level

 -- Sandro Tosi <morph@debian.org>  Sun, 01 Mar 2015 21:46:30 +0000

abs-guide (6.6-1) unstable; urgency=low

  * New upstream release; thanks to Sébastien Villemot for the report;
    Closes: #733155
  * debian/control
    - bump Standards-Version to 3.9.5 (no changes needed)
    - use packaging repository canonical URLs

 -- Sandro Tosi <morph@debian.org>  Wed, 01 Jan 2014 12:26:22 +0100

abs-guide (6.5-1) unstable; urgency=low

  * New upstream release
  * debian/watch
    - updated
  * debian/abs-guide.lintian-overrides
    - updated for new upstream code
  * debian/control
    - bump Standards-Version to 3.9.3 (no changes needed)

 -- Sandro Tosi <morph@debian.org>  Sun, 03 Jun 2012 10:57:27 +0200

abs-guide (6.2-1) unstable; urgency=low

  * New upstream release; Closes: #441278
  * debian/control
    - adopting package; Closes: #577181
    - removed bzip2 from b-d-i, no more needed
    - debhelper has to be in b-d
    - bump Standards-Version to 3.8.4 (no changes needed)
    - added Homepage field
    - added misc:Depends to Depends
    - added Vcs-{Browser, Git} field
  * debian/rules
    - removed the unpacking stuff, we now use an alrqady unpacked source
    - added more example scripts to fix
  * debian/{compat, control, rules}
    - switched to debhelper 7
  * debian/watch
    - updated to new upstream location; thanks to Raphael Geissert for the
      report; Closes: #453596
  * debian/copyright
    - updated with new location and copyright/license notices
  * debian/source/format
    - set source package format to 1.0 explicitly
  * debian/abs-guide.doc-base
    - set Section field correctly
  * debian/{copyright, abs-guide.doc-base}
    - updated upstream email
  * debian/abs-guide.lintian-overrides
    - added override for "known syntax-errored" scripts

 -- Sandro Tosi <morph@debian.org>  Sun, 09 May 2010 17:49:52 +0200

abs-guide (4.1-2) unstable; urgency=low

  * Orphan the package.

 -- Matthias Klose <doko@debian.org>  Sat, 10 Apr 2010 12:25:26 +0200

abs-guide (4.1-1) unstable; urgency=low

  * New upstream version.

 -- Matthias Klose <doko@debian.org>  Sun, 29 Oct 2006 02:37:03 +0100

abs-guide (4.0-1) unstable; urgency=low

  * New upstream version.

 -- Matthias Klose <doko@debian.org>  Sun,  2 Jul 2006 13:22:28 +0200

abs-guide (3.7-1) unstable; urgency=low

  * New upstream version.

 -- Matthias Klose <doko@debian.org>  Sun, 23 Oct 2005 16:35:23 +0000

abs-guide (3.4-1) unstable; urgency=medium

  * New upstream version.

 -- Matthias Klose <doko@debian.org>  Sun,  8 May 2005 23:13:06 +0200

abs-guide (3.2-1) unstable; urgency=low

  * New upstream version.

 -- Matthias Klose <doko@debian.org>  Tue, 15 Feb 2005 01:24:25 +0100

abs-guide (3.1-1) unstable; urgency=low

  * New version

 -- Matthias Klose <doko@debian.org>  Sun, 21 Nov 2004 15:20:30 +0100

abs-guide (3.0-1) unstable; urgency=low

  * New version.

 -- Matthias Klose <doko@debian.org>  Sun,  3 Oct 2004 14:41:53 +0200

abs-guide (2.8-1) unstable; urgency=low

  * New version.

 -- Matthias Klose <doko@debian.org>  Fri, 16 Jul 2004 23:12:38 +0200

abs-guide (2.6-1) unstable; urgency=low

  * New version.

 -- Matthias Klose <doko@debian.org>  Wed, 14 Apr 2004 00:51:44 +0200

abs-guide (2.5-1) unstable; urgency=low

  * New version.

 -- Matthias Klose <doko@debian.org>  Tue,  2 Mar 2004 22:57:51 +0100

abs-guide (2.1-1) unstable; urgency=low

  * Initial Release (closes: #115637, 154248).

 -- Matthias Klose <doko@debian.org>  Wed, 24 Sep 2003 09:29:19 +0200
