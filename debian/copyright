Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: abs-guide
Source: http://www.tldp.org/LDP/abs/html/

Files: *
Copyright: no copyright
License: public-domain
 The Advanced Bash Scripting Guide is herewith granted to the PUBLIC DOMAIN.
 This has the following implications and consequences.
 .
 A.  All previous releases of the Advanced Bash Scripting Guide
     are as well granted to the Public Domain.
 .
 A1. All printed editions, whether authorized by the author or not,
     are as well granted to the Public Domain. This legally overrides
     any stated intention or wishes of the publishers. Any statement
     of copyright is void and invalid.
     THERE ARE NO EXCEPTIONS TO THIS.
 .
 A2. Any release of the Advanced Bash Scripting Guide, whether in
     electronic or print form is granted to the Public Domain by the
     express directive of the author and previous copyright holder, Mendel
     Cooper. No other person(s) or entities have ever held a valid copyright.
 .
 B.  As a Public Domain document, unlimited copying and distribution rights
     are granted. There can be NO restrictions. If anyone has published or will
     in the future publish an original or modified version of this document,
     then only additional original material may be copyrighted. The core
     work will remain in the Public Domain.
 .
 By law, distributors and publishers (including on-line publishers) are
 prohibited from imposing any conditions, strictures, or provisions on this
 document, any previous versions, or any derivative versions. The author asserts
 that he has not entered into any contractual obligations that would alter the
 foregoing declarations.
 .
 Essentially, you may freely distribute this book or any derivative thereof in
 electronic or printed form. If you have previously purchased or are in
 possession of a printed copy of a current or previous edition, you have the
 LEGAL RIGHT to copy and/or redistribute it, regardless of any copyright notice.
 Any copyright notice is void.
 .
 Additionally, the author wishes to state his intention that:
 .
 1 If you copy or distribute this book, kindly DO NOT
 2 use the materials within, or any portion thereof, in a patent or copyright
 3 lawsuit against the Open Source community, its developers, its
 4 distributors, or against any of its associated software or documentation
 5 including, but not limited to, the Linux kernel, Open Office, Samba,
 6 and Wine. Kindly DO NOT use any of the materials within
 7 this book in testimony or depositions as a plaintiff's "expert witness" in
 8 any lawsuit against the Open Source community, any of its developers, its
 9 distributors, or any of its associated software or documentation.
 .
 A Public Domain license essentially does not restrict ANY legitimate
 distribution or use of this book. The author especially encourages its
 (royalty-free!) use for classroom and instructional purposes.
 .
 To date, limited print rights (Lulu edition) have been granted to one
 individual and to no one else. Neither that individual nor Lulu holds or ever
 has held a valid copyright.
 .
 It has come to the attention of the author that unauthorized electronic and
 print editions of this book are being sold commercially on itunes®, amazon.com
 and elsewhere. These are illegal and pirated editions produced without the
 author's permission, and readers of this book are strongly urged not to
 purchase them. In fact, these pirated editions are now legal, but necessarily
 fall into the Public Domain, and any copyright notices contained within them
 are invalid and void.
 .
 The author produced this book in a manner consistent with the spirit of the LDP Manifesto.
 .
 Linux is a trademark registered to Linus Torvalds.
 Fedora is a trademark registered to Red Hat.
 Unix and UNIX are trademarks registered to the Open Group.
 MS Windows is a trademark registered to the Microsoft Corp.
 Solaris is a trademark registered to Oracle, Inc.
 OSX is a trademark registered to Apple, Inc.
 Yahoo is a trademark registered to Yahoo, Inc.
 Pentium is a trademark registered to Intel, Inc.
 Thinkpad is a trademark registered to Lenovo, Inc.
 Scrabble is a trademark registered to Hasbro, Inc.
 Librie, PRS-500, and PRS-505 are trademarks registered to Sony, Inc.
 .
 All other commercial trademarks mentioned in the body of this work are
 registered to their respective owners.

Files: debian/*
Copyright: 2010-2019 Sandro Tosi <morph@debian.org>
License: GPL-2+
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in "/usr/share/common-licenses/GPL-2"
